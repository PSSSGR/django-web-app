# Django Web App
The Smart Role-Playing Game Board is a physical game board that maps piece movements
using magnetic switches accompanied by magnetic bases onto the user interface (UI). It also
provides a method for users to easily design LED maps. The Smart Role-Playing Game Board
is devised with convenience in mind, enhancing the experience for remote users by mimicking
the state of the board onto the UI. The main objective is to allow a remote user to access the
functions of the user interface in real time.
