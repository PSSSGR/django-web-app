var taken_tiles = [];
$(document).ready(function(){
console.log("Connected");
  var button_wrapper = document.getElementsByClassName("buttongrid");
  var myHTML = '';
  for (var i = 0; i < 100; i++) {
      myHTML += '<button id="' + i + '"></button>';
  }
  button_wrapper[0].innerHTML = myHTML;
  $("button").click(function(){
    $(this).toggleClass("selected");
  });

  $("a").click(function(){
  	if ($(this).attr("id") == "wall"){
        change_color($(this).attr("id"));
    }
    else if ($(this).attr("id") == "tree"){
        change_color($(this).attr("id"));
    }
    else if ($(this).attr("id") == "rock"){
        change_color($(this).attr("id"));
    }
    else if ($(this).attr("id") == "water"){
        change_color($(this).attr("id"));
    }
    else if ($(this).attr("id") == "fire"){
        change_color($(this).attr("id"));
    }
    else if ($(this).attr("id") == "deselect"){
        change_color($(this).attr("id"));
    }
  });

//This code is for testing, it adds in elements that are "triggered" meaning there would be pieces on them
//Uncomment below to use test code

//$("button").each(function(){
//    if ($(this).attr("id")%13 == "6"){
//        $(this).addClass("triggered");
//    }
//})

// end of test code

//function to change color of selected squares
function change_color(id){
    $("button").each(function() {
        var classes = "";
        if ($(this).hasClass("selected")){
            if (!($(this).hasClass(id))){
                  classes = id;
            }
            if ($(this).hasClass("triggered")) {
                classes += " triggered";
            }
            $(this).attr("class", classes);
        }
   });
}

// ======Start of adding and listing pieces======
var charactersNodelist = document.getElementsByTagName("LI");
var i;
for (i = 0; i < charactersNodelist.length; i++) {
  var span = document.createElement("SPAN");
  var txt = document.createTextNode("\u00D7");
  span.className = "close";
  span.appendChild(txt);
  charactersNodelist[i].appendChild(span);
}

// Add a "checked" symbol when clicking on a list item
var list = document.querySelector('ul');
list.addEventListener('click', function(ev) {
  if (ev.target.tagName === 'LI') {
    ev.target.classList.toggle('checked');
  }
}, false);
});

// Create a new list item when clicking on the "Add" button
function newElement() {
  var li = document.createElement("li");
  var inputValue = document.getElementById("characterInput").value;
  var letters = ["A","B","C","D","E","F","G","H","I","J"];
  var tile_list = [];
  var character_added = false;

  $("button").each(function() {
        if ($(this).hasClass("selected")){
            if ($(this).hasClass("triggered")) {
                tile_list.push(letters[Math.floor($(this).attr("id")/10)] + ($(this).attr("id")%10+1));
            }
        }
   });
  var character_string = inputValue

  for(var i = 0; i < tile_list.length; i++) {
    character_string += ", " + tile_list[i];
  }

  var t = document.createTextNode(character_string);
  li.appendChild(t);

  if (inputValue === '') {
    alert("You must input a name.");
  }
  else if (tile_list.length == 0) {
    alert("You must select a piece to add.");
  }
  else if(tile_list.length > 1){
    alert("Please only select 1 element.")
  }
  else if(taken_tiles.includes(tile_list[0])){
    alert("Tile already taken.");
  }
  else {
    document.getElementById("character_list").appendChild(li);
    character_added = true;
    taken_tiles.push(tile_list[0]);
  }

  if(character_added){
    document.getElementById("characterInput").value = "";
  }

  var span = document.createElement("SPAN");
  var txt = document.createTextNode("\u00D7");
  span.className = "close";
  span.appendChild(txt);
  li.appendChild(span);

  for (var i = 0; i < close.length; i++) {
    close[i].onclick = function() {
      var div = this.parentElement;
      div.style.display = "none";
      var location = "";
      if (this.parentElement.textContent.slice(-2,-1) == "0"){
        location = this.parentElement.textContent.slice(-4,-1);
      }
      else{
        location = this.parentElement.textContent.slice(-3,-1);
      }
      for (var i = 0; i < taken_tiles.length; i++) {
        if (taken_tiles[i] == location) {
            taken_tiles.splice(i,1)
        }
      }
    }
  }
};
// Click on a close button to hide the current list item
var close = document.getElementsByClassName("close");
var i;
for (i = 0; i < close.length; i++) {
  close[i].onclick = function() {
    var div = this.parentElement;
    div.style.display = "none";
    var location = "";
    if (this.parentElement.textContent.slice(-2,-1) == "0"){
        location = this.parentElement.textContent.slice(-4,-1);
    }
    else{
        location = this.parentElement.textContent.slice(-3,-1);
    }
    for (var i = 0; i < taken_tiles.length; i++) {
        if (taken_tiles[i] == location) {
            taken_tiles.splice(i,1)
        }
    }
  }
};
// ======End of adding and listing pieces======