from django.shortcuts import render
from django.http import HttpResponse
from .models import Board_class
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
import json
from django.http import JsonResponse

# Create your views here.
@csrf_exempt
def home(request):
	return render(request, 'blog/home.html')


@csrf_exempt
def data(request):
	context = dict()
	if request.method == 'POST':
		print(request)
		# for i in range(100): 
		# 	square_piece = json.loads(dict(request.POST)['undefined'][i]);
		# 	db = Board_class(piece_id=square_piece["piece_id"], state = square_piece["triggered_state"] , 
		# 		color=square_piece["env_color"], label = square_piece["character_info"])
		# 	db.save()
		# return HttpResponse("DB created")
	else:
		data = list(Board_class.objects.values())
		return JsonResponse(data, status=200, safe = False)
